﻿const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const config = require('./config') // get our config file
// const mfrc522 = require('MFRC522-node')
// const rfid = require('./SPI-Py/rfid.js')


// =======================
// configuration =========
// =======================
var port = process.env.PORT || 3000

// app.set('superSecret', config.secret)
// app.use(express.static(__dirname + '/public'))


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('dev'))

// app.disable('x-powered-by')
// app.set('etag', false)
//app.use(methodOverride());                  // simulate DELETE and PUT

// Middleware para coletar o ip do usuário
// app.use(require('request-ip').mw())


// LOADING CONTROLLERS (WITH ROUTES)
// =============================================================================
app.use('/uncouple', require('./api/uncouple/routes').init())
app.use('/openrelay', require('./api/openrelay/routes').init())

    // =======================
    // start the server ======
    // =======================
app.listen(port)
console.log('Terminal operando na porta %s' + port)

// mfrc522.start( new rfid.Callback() )
