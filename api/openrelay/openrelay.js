const express = require('express')
const app = express()
var gpio = require('rpi-gpio');
// const raspi = require('raspi-io')
// const five = require('johnny-five')

var delay = 800;

gpio.setup(7, gpio.DIR_IN);
gpio.setup(11, gpio.DIR_IN);

function post(data, callback) {
	
	if(data.car_position_totems == 1) {
    	var pin = 7;
	} else if (data.car_position_totems == 2) {
		var pin = 11;
	}
	
	gpio.setup(pin, gpio.DIR_OUT, on);
	
    function on() {    
        setTimeout(function () {
    			gpio.write(pin, 1);
    	  }, delay);
    	  gpio.write(pin, 0);   
    	  return callback(null, {status: 'sucess'});
        return;
    }    
}
/*
const board = new five.Board({
    io: new raspi()
  });
var relay1, relay2, status = ''
  board.on('ready', () => {
    // relays on pin 7 on header P1 (GPIO7)
    relay1 = new five.Relay('P1-7');
    relay2 = new five.Relay('P1-11');
  });

function post (data, callback) {
 if(data.car_position_totems == 1) {
  relay1.toggle();
  status = 'Carrinho numero 1 liberado'
 } else if(data.car_position_totems == 2) {
  relay2.toggle();
  status = 'Carrinho numero 2 liberado'
 }

  return callback(null, {status: status})
}
*/
module.exports = {
  post
}
