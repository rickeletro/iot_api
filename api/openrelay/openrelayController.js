const openrelay   = require('./openrelay.js')
// const verify   = require('../../verify.js')

function post (req, res, next) {
  openrelay.post(req.body, (err, data) => {
    if (err) {
      res.status(500).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post
}
