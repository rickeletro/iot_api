function init() {

    const express 		      = require("express")
    const openrelayController   = require("./openrelayController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            openrelayController.post
        )

    return routes

}

module.exports = {
    init
}
