const uncouple   = require('./uncouple.js')
const verify   = require('../../verify.js')

function post (req, res, next) {
  uncouple.post(req.body, (err, data) => {
    if (err) {
      res.status(500).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post
}
