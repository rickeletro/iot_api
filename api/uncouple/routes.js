function init() {

    const express 		      = require("express")
    const uncoupleController   = require("./uncoupleController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            uncoupleController.post
        )

    return routes

}

module.exports = {
    init
}
